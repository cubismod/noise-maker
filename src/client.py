import time
from queue import Queue
import simpleaudio
import logging
from pydub import generators
from threading import Thread

logging.basicConfig(level=logging.INFO)


# generate 1 min
# which is looped 1
def _gen_noise():
    seg_len = 10000 # 10 s
    vol = -20
    # fade_time = 5000

    gen = generators.WhiteNoise()
    # start = gen.to_audio_segment(seg_len, vol).fade_in(fade_time)
    middle = gen.to_audio_segment(seg_len, vol)
    # end = gen.to_audio_segment(seg_len, vol).fade_out(fade_time)

    # neatly crossfade all the sounds
    # sound = start.append(middle.append(end))
    return middle


def _play_noise(noise):
    return simpleaudio.play_buffer(
        noise.raw_data,
        num_channels=noise.channels,
        bytes_per_sample=noise.sample_width,
        sample_rate=noise.frame_rate
    )


def _proc_play(stop_queue: Queue):
    stop = True
    noise = _gen_noise()
    while True:
        if not stop_queue.empty():
            stop = stop_queue.get()
        if not stop:
            # from here on out we just play that noise
            _play_noise(noise)
            time.sleep(9.97)    # this is the best delay that works on my rasp pi 3b
        else:
            time.sleep(5)


class AudioClient:
    running = False
    '''
    Spawns a new thread to handle consistently
    looping the audio.'''

    def __init__(self):
        self.stop_queue = Queue()
        play_thread = Thread(target=_proc_play, args=(self.stop_queue,))
        play_thread.start()

    @staticmethod
    def _chime(reverse):
        pg = generators.Sawtooth(200)
        seg_1 = pg.to_audio_segment(300, -25)
        pg2 = generators.Sawtooth(650)
        seg_2 = pg2.to_audio_segment(200, -25)
        combined = seg_1 + seg_2
        if reverse:
            combined = seg_2 + seg_1
        # play it async this way rather than the
        # pydub implementation
        simpleaudio.play_buffer(
            audio_data=combined.raw_data,
            num_channels=combined.channels,
            bytes_per_sample=combined.sample_width,
            sample_rate=combined.frame_rate
        )

    def toggle(self):
        # chime to indicate command recieved
        if not self.running:
            self._chime(False)
            # reset this flag
            self.running = True
            self.stop_queue.put(False)
        else:
            self._chime(True)
            self.running = False
            self.stop()

    def stop(self):
        # this should then terminate the process
        self.stop_queue.put(True)
