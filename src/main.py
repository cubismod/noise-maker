from os import stat_result
from client import AudioClient
import paho.mqtt.client as mqtt
import config
import random
import logging
import json

logging.basicConfig(level=config.log_lvl)

ac = AudioClient()

broker = config.broker
port = config.port
topic = 'cubis/noise-maker'
client_id = f'noise-maker-{random.randint(0, 500)}'
status = 'cubis/noise-maker/status'


def on_connect(client, _userdata, _flags, rc):
    logging.info(f'connected to MQTT with rc {str(rc)}')
    client.subscribe(topic)
    client.subscribe(status)


def on_message(_client, _userdata, msg):
    logging.info(f'msg received {str(msg.payload)}')
    payload = str(msg.payload)
    if payload == f'b\'{config.tgl_key}\'':
        logging.info(f'received {config.tgl_key} msg')
        ac.toggle()
        if ac.running:
            # send a status to home assistant binary sensor
            mc.publish(status, json.dumps({'id': client_id, 'msg': 'on'}))
        else:
            mc.publish(status, json.dumps({'id': client_id, 'msg': 'off'}))


def on_disconnect(_client, _userdata, rc):
    if rc != 0:
        logging.warning(f'unexpected MQTT disconnect with rc {str(rc)}')


mc = mqtt.Client()
mc.on_connect = on_connect
mc.on_message = on_message
mc.on_disconnect = on_disconnect

mc.connect(broker, port)

print('\nnoise-maker')
print('by cubis 2022')
print('send the message "toggle" over MQTT to topic cubis/noise-maker to toggle the noise')
print('ctrl-c to quit\n')

mc.publish(status, json.dumps({'id': client_id, 'msg': 'off'}))
mc.loop_forever()
