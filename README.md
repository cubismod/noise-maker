## noise-maker
Play noise to a Raspberry Pi over the MQTT protocol.

## Architecture
Alsa with simpleaudio in Python.
MQTT with Paho MQTT

## References
https://www.emqx.com/en/blog/how-to-use-mqtt-in-python